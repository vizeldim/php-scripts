<?php declare(strict_types=1);


namespace App;


use App\Invoice\BusinessEntity;
use App\Invoice\Item;
use Dompdf\Dompdf;

class Renderer extends Dompdf
{
    private function renderBEntity(BusinessEntity $e) : string{
        $le = "<br>";
        $dle = $le . $le;
        return $e->getName()                 . $le .
               $e->getAddress()->getStreet() . " " .
               $e->getAddress()->getNumber() . $le .
               substr_replace(
                   $e->getAddress()->getZipCode(),
                   " ",
                   3,
                   0)                  . " " .
               $e->getAddress()->getCity()   . $dle .
               $e->getVatNumber()            . $dle .
               $e->getAddress()->getPhone()  . $le .
               $e->getAddress()->getEmail();
    }


    private function renderItems($items, string ... $labels) : string{
        $output = "";
        $output .= "<tr>";
        foreach ($labels as $label){
            $output .= "<th>" . $label . "</th>";
        }
        $output .= "</tr>";

        foreach ($items as $item) {
            $output .= "<tr>";
            $output .= "<td>". $item->getDescription() ."</td>";
            $output .= "<td class='num'>". $item->getQuantity() ."</td>";
            $output .= "<td class='num'>". number_format($item->getUnitPrice(),2,","," ") ."</td>";
            $output .= "<td class='num'>". number_format($item->getTotalPrice(), 2,","," ") ."</td></tr>";
            $output .= "</tr>";
        }
        return $output;
    }

    public function makeInvoice(Invoice $invoice): string
    {
        $html = "
        <html>
            <head>
                <style>
                    body{
                        font-family: arial,sans-serif;
                        font-size: 14px;
                    }
                    .invoice{
                        width: 86%;
                        margin: 50px auto;
                    }
                    
                    h1{
                        font-weight: normal;
                        font-size: 14px;
                        padding-bottom: 15px;
                    }  
                    table{
                        width: 100%;
                        table-layout: fixed ;
                        border-collapse: collapse;
                    }
                    table, tr, td, th{ 
                        border: 0.5px solid black;
                        text-align: left;
                        vertical-align: top;
                     }
                     .num{
                        text-align: right; 
                     }
                     td,th{
                        padding: 5px;
                     } 
                </style>
            </head>
            <body>
                <div class='invoice'>
                    <h1>FAKTURA - DOKLAD c. " . $invoice->getNumber() . "</h1>
                    <table>
                        <tr>
                            <td>
                                <b>Dodavatel</b><br><br>
                                " . $this->renderBEntity($invoice->getSupplier()) . "
                            </td>
                            <td>
                                <b>Odberatel</b><br><br>
                                " . $this->renderBEntity($invoice->getCustomer()) . "
                            </td>           
                        </tr>
                    </table><br>
                    <table>
                            " . $this->renderItems($invoice->getItems(), "Polozka", "Pocet m.j.", "Cena za m.j.", "Celkem") . "
                            <tr>
                                <th colspan='3'>Celkem</th>
                                <th class='num'>". number_format($invoice->getTotalPrice(), 2,","," ") ."</th>
                            </tr>
                    </table>
                </div>
            </body>
        </html>
        ";

        $this->setPaper('A4', 'portrait');
        $this->loadHtml($html);
        $this->render();

        return $this->output();
    }
}
