<?php

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

require __DIR__.'/vendor/autoload.php';

function text(Crawler $crawler, string $selector)
{
    $new = $crawler->filter($selector);
    if (count($new)) {
        return trim($new->text());
    }

    return null;
}


function cmp($a, $b){
    preg_match_all('!\d+!', $a["price"], $matchesA);
    $varA = implode('', $matchesA[0]);
    $valA = floatval($varA);

    preg_match_all('!\d+!', $b["price"], $matchesB);
    $varB = implode('', $matchesB[0]);
    $valB = floatval($varB);

    if ($valA<$valB)
        return -1;
    if ($valB<$valA)
        return 1;
    else
        return 0;
}

/**
 * @param string $query - query string e.g. 'Beats Studio 3'
 * @return array
 */
function scrape(string $query)
{
    $client = new Client();

    $alzaSearch = "https://www.alza.cz/search.htm?exps=";
    $crawler = $client->request("GET",$alzaSearch.$query);


    $resultsAlza = $crawler
        ->filter("div.browsingitemcontainer div.browsingitem")
        ->each(function($node){
            $name = $node->filter(".fb .name")->first();
            if ($name!==null){$product['name']=$name->text();}

            $price = $node->filter(".priceInner .c2")->first();
            if ($price!==null){ $product['price'] = $price->text(); }

            $link = $node->filter("a.browsinglink")->first();
            if ($link!==null){ $product['link'] = "https://www.alza.cz".$link->attr('href'); }

            $product['eshop'] = "alza";

            $description = $node->filter(".Description")->first();
            if ($description!==null) { $product['description'] = $description->text(); }

            return $product;
        });

    $mallSearch = "https://www.mall.cz/hledej?s=";
    $crawler = $client->request("GET",$mallSearch.$query);

    $resultsMall = $crawler
        ->filter("div.product-list article")
        ->each(function($node){

            $name = $node->filter("h3.lst-product-item-title a")->first();
            if ($name!==null){$product['name']=$name->text();}

            $price = $node->filter(".lst-product-item-price-value")->first();
            if ($price!==null){ $product['price'] = $price->text(); }

            $link = $node->filter("h3.lst-product-item-title a")->first();
            if ($link!==null){ $product['link'] = "https://www.mall.cz".$link->attr('href'); }

            $product['eshop'] = "mall";

            $description = $node->filter(".lst-product-item-description")->first();
            if ($description!==null) { $product['description'] = $description->text(); }

            return $product;
        });

    $results = array_merge($resultsAlza, $resultsMall);
    usort($results, "cmp");
    return $results;
}


/**
 * @param string $query   - query string e.g. 'Beats Studio 3'
 * @param array  $results - input product collection
 * @return array
 */
function filter(string $query, array $results)
{
    $words = explode(" ", $query);
    foreach ($results as $key=>$result) {
        foreach ($words as $word){
            if (mb_stripos($result['name'],$word)===false &&
                mb_stripos($result['description'],$word)===false)
            {
                unset($results[$key]);
                break;
            }
        }
    }
    return $results;
}

/**
 * input array $results show contain following keys
 * - 'name'
 * - 'price'
 * - 'link' - link to product detail page
 * - 'eshop' - eshop identifier e.g. 'alza'
 * - 'description'
 *
 * @param array $results
 */
function printResults(array $results, bool $includeDescription = false)
{
    $width = [
        'name' => 0,
        'price' => 0,
        'link' => 0,
        'eshop' => 0,
        'description' => 0,
    ];
    foreach ($results as $result) {
        foreach ($result as $key => $value) {
            $width[$key] = max(mb_strlen($value), $width[$key]);
        }
    }
    echo '+'.str_repeat('-', 2 + $width['name']);
    echo '+'.str_repeat('-', 2 + $width['price']);
    echo '+'.str_repeat('-', 2 + $width['link']);
    echo '+'.str_repeat('-', 2 + $width['eshop'])."+\n";
    foreach ($results as $result) {

        echo '| '.$result['name'].str_repeat(' ', $width['name'] - mb_strlen($result['name'])).' ';
        echo '| '.$result['price'].str_repeat(' ', $width['price'] - mb_strlen($result['price'])).' ';
        echo '| '.$result['link'].str_repeat(' ', $width['link'] - mb_strlen($result['link'])).' ';
        echo '| '.$result['eshop'].str_repeat(' ', $width['eshop'] - mb_strlen($result['eshop'])).' ';
        echo "|\n";
        echo '+'.str_repeat('-', 2 + $width['name']);
        echo '+'.str_repeat('-', 2 + $width['price']);
        echo '+'.str_repeat('-', 2 + $width['link']);
        echo '+'.str_repeat('-', 2 + $width['eshop'])."+\n";
        if ($includeDescription) {
            echo '| '.$result['description'].str_repeat(' ',
                    max(0, 7 + $width['name'] + $width['price'] + $width['link'] - mb_strlen($result['description'])));
            echo "|\n";
            echo str_repeat('-', 10 + $width['name'] + $width['price'] + $width['link'])."\n";
        }
    }
}

// MAIN
if (count($argv) !== 2) {
    echo "Usage: php run.php <query>\n";
    exit(1);
}

$query = $argv[1];
$results = scrape($query);
$results = filter($query, $results);
printResults($results);
