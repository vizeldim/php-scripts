<?php

namespace Iterator;

use Node;

abstract class AbstractOrderIterator implements \Iterator
{
    protected $nodes = [];
    protected $key;

    public function __construct(Node $root)
    {
        $this->key = 0;
        $this->saveOrdered($root);
    }

    public abstract function saveOrdered(?Node $root);

    public function current(): ?Node
    {
        return $this->valid()? $this->nodes[$this->key] : null;
    }

    public function next(): void
    {
        ++$this->key;
    }

    /**
     * @return bool|float|int|string|null
     */
    public function key()
    {
        return $this->key;
    }

    public function valid(): bool
    {
        return array_key_exists($this->key, $this->nodes);
    }

    public function rewind(): void
    {
        $this->pos = 0;
    }
}
