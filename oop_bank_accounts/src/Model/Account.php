<?php declare(strict_types=1);

namespace App\Model;

use App\Db;

class Account
{
    /** @var integer */
    protected $id;

    /** @var string */
    protected $number;

    /** @var string */
    protected $code;

    /**
     * Account constructor.
     *
     * @param int $id
     * @param string $number
     * @param string $code
     */
    public function __construct(int $id, string $number, string $code)
    {
        $this->id = $id;
        $this->number = $number;
        $this->code = $code;
    }

    /**
     * Creates DB table using CREATE TABLE ...
     */

    public static function createTable(): void
    {
        $db = Db::get();
        $db->query('CREATE TABLE IF NOT EXISTS `account` (
            `id`     INTEGER    PRIMARY KEY AUTOINCREMENT,
            `number` TEXT       NOT NULL,
            `code`   TEXT       NOT NULL
        )');
    }

    /**
     * Drops DB table using DROP TABLE ...
     */
    public static function dropTable(): void
    {
        $db = DB::get();
        $db -> query('DROP TABLE IF EXISTS `account`');
    }

    /**
     * Find account record by number and bank code
     *
     * @param string $number
     * @param string $code
     * @return Account|null
     */
    public static function find(string $number, string $code): ?self
    {
        $db = DB::get();
        $stat = $db -> prepare("SELECT `id`,`number`, `code` FROM `account` WHERE `number` = ? AND `code` = ?");
        $stat -> execute([$number, $code]);
        $data = $stat -> fetch(\PDO::FETCH_ASSOC);

        if ($data){
            return new Account(
                intval($data["id"]),
                $data["number"],
                $data["code"]);
        }
        return null;
    }

    /**
     * Find account record by id
     * 
     * @param int $id
     * @return static|null
     */
    public static function findById(int $id): ?self
    {
        $db = DB::get();
        $stat = $db ->prepare("SELECT `id`,`number`, `code` FROM `account` WHERE `id` = ? ");
        $stat -> execute([$id]);
        $data = $stat ->fetch(\PDO::FETCH_ASSOC);

        if ($data){
            return new Account(
                intval($data["id"]),
                $data["number"],
                $data["code"]
            );
        }
        return null;
    }

    /**
     * Inserts new account record and returns its instance; or returns existing account instance
     *
     * @param string $number
     * @param string $code
     * @return static
     */
    public static function findOrCreate(string $number, string $code): self
    {
        $acc = self::find($number,$code);
        if ($acc == null){
            $db = DB::get();
            $stat = $db ->prepare('INSERT INTO `account`(`number`, `code`) VALUES (?,?)');
            $stat -> execute([$number,$code]);
            $acc = new Account(
                intval(DB::get()->lastInsertId()),
                $number,
                $code
            );
        }
        return $acc;
    }

    /**
     * Returns iterable of Transaction instances related to this Account, consider both transaction direction
     *
     * @return iterable<Transaction>
     */
    public function getTransactions(): iterable
    {
        $db = DB::get();
        $stat = $db ->prepare('SELECT * FROM `transaction` WHERE `from`=? OR `to`=?');
        $stat -> execute([$this->getId(),$this->getId()]);
        $data = $stat -> fetchAll();

        $ta = [];
        foreach ($data as $row){
            $ta[] = new Transaction(
                self::findById(intval($row["from"])),
                self::findById(intval($row["to"])),
                floatval($row["amount"])
            );
        }

        return $ta;
    }

    /**
     * Returns transaction sum (using SQL aggregate function). Treat outgoing transactions as 'minus' and incoming as 'plus'.
     *
     * @return float
     */
    public function getTransactionSum(): float
    {
        $sum = 0;
        $db = DB::get();
        $stat = $db ->prepare('SELECT SUM(`amount`) FROM `transaction` WHERE `to`=?' );
        $stat -> execute([$this->getId()]);
        $totalIn = $stat -> fetch();
        if ($totalIn!=FALSE)
        { $sum += floatval($totalIn[0]); }

        $stat = $db ->prepare('SELECT SUM(`amount`) FROM `transaction` WHERE `from`=?' );
        $stat -> execute([$this->getId()]);
        $totalOut = $stat -> fetch();
        if ($totalOut!=FALSE)
        { $sum -= floatval($totalOut[0]); }

        return $sum;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Account
     */
    public function setId(int $id): Account
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Account
     */
    public function setNumber(string $number): Account
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Account
     */
    public function setCode(string $code): Account
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Account string representation
     *
     * @return string
     */
    public function __toString()
    {
        return "{$this->number}/{$this->code}";
    }
}
