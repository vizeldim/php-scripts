<?php

namespace HW\Lib;

use http\Exception\InvalidArgumentException;

class LinkedList
{
    /** @var LinkedListItem|null */
    protected $first = null;

    /** @var LinkedListItem|null */
    protected $last = null;

    /**
     * @return LinkedListItem|null
     */
    public function getFirst(): ?LinkedListItem
    {
        return $this->first;
    }

    /**
     * @param LinkedListItem|null $first
     * @return LinkedList
     */
    private function setFirst(?LinkedListItem $first): LinkedList
    {
        $this->first = $first;

        return $this;
    }

    /**
     * @return LinkedListItem|null
     */
    public function getLast(): ?LinkedListItem
    {
        return $this->last;
    }

    /**
     * @param LinkedListItem|null $last
     * @return LinkedList
     */
    private function setLast(?LinkedListItem $last): LinkedList
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Place new item at the begining of the list
     *
     * @param string $value
     * @return LinkedListItem
     */
    public function prependList(string $value)
    {
        $item = new LinkedListItem($value);
        $second = $this->getFirst();
        $this->setFirst($item);
        $item->setNext($second);
        if ($second!=null)
            $second->setPrev($item);
        else
            $this->setLast($item);

        return $item;
    }

    /**
     * Place new item at the end of the list
     *
     * @param string $value
     * @return LinkedListItem
     */
    public function appendList(string $value)
    {
        $item = new LinkedListItem($value);
        $penultimate = $this->getLast();
        $this->setLast($item);
        $item->setPrev($penultimate);
        if ($penultimate!=null){
            $penultimate->setNext($item);
        }
        else
        {
            $this->setFirst($item);
        }
        return $item;
    }

    /**
     * Insert item before $nextItem and maintain continuity
     *
     * @param LinkedListItem $nextItem
     * @param string         $value
     * @return LinkedListItem
     */
    public function prependItem(LinkedListItem $nextItem, string $value)
    {
        $findItem = $this->first;
        $found = false;
        while($findItem!=null){
            if($findItem === $nextItem)
            {
                $found = true;
                break;
            }
            $findItem = $findItem->getNext();
        }
        if (!$found)
        { throw new \InvalidArgumentException(); }

        $item = new LinkedListItem($value);

        if ($nextItem === $this->first)
        { $this->setFirst($item); }

        $item->setNext($nextItem);
        $item->setPrev($nextItem->getPrev());
        if ($nextItem->getPrev()!=null)
        {
            $nextItem->getPrev()->setNext($item);
        }
        $nextItem->setPrev($item);


        return $item;
    }

    /**
     * Insert item after $prevItem and maintain continuity
     *
     * @param LinkedListItem $prevItem
     * @param string         $value
     * @return LinkedListItem
     */
    public function appendItem(LinkedListItem $prevItem, string $value)
    {
        $findItem = $this->first;
        $found = false;
        while($findItem!=null){
            if($findItem === $prevItem)
            {
                $found = true;
                break;
            }
            $findItem = $findItem->getNext();
        }
        if (!$found)
        { throw new \InvalidArgumentException(); }


        $item = new LinkedListItem($value);

        if ($prevItem === $this->last)
        { $this->setLast($item); }

        $item->setPrev($prevItem);
        $item->setNext($prevItem->getNext());
        if ($prevItem->getNext()!=null)
        {
            $prevItem->getNext()->setPrev($item);
        }
        $prevItem->setNext($item);

        return $item;
    }
}
