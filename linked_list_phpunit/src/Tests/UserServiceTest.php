<?php


namespace HW\Tests;

use HW\Lib\Storage;
use HW\Lib\UserService;
use PHPUnit\Framework\TestCase;
use stdClass;

class UserServiceTest extends TestCase
{
    protected $userService;
    /**
     * @var Storage
     */
    protected $mockStorage;
    public function setUp(): void
    {
        parent::setUp();
        $this->mockStorage = $this->createMock(Storage::class);
        $this->userService = new UserService($this->mockStorage);
    }

    public function testCreate()
    {
        self::assertInstanceOf(UserService::class, $this->userService);
    }

    public function testCreateGetUser()
    {
        $mockUser = new stdClass();
        $mockUser = json_encode([ 'username' => 'TestName', 'email' => 'TestMail']);

        $this->mockStorage->method('get')->willReturn($mockUser);
        $id = $this->userService->createUser('TestName', 'TestMail');
        $this->assertEquals('TestName',$this->userService->getUsername($id));
        $this->assertEquals('TestMail',$this->userService->getEmail($id));

    }

    public function testGetUserNotExist()
    {
        $mockUser = null;
        $this->mockStorage->method('get')->willReturn($mockUser);

        $this->assertEquals(null,$this->userService->getUsername(0));
        $this->assertEquals(null,$this->userService->getEmail(0));

    }

}