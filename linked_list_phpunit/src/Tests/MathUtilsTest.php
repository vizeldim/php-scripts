<?php


namespace HW\Tests;


use HW\Lib\MathUtils;
use HW\Lib\NoResultException;
use PHPUnit\Framework\TestCase;

class MathUtilsTest extends TestCase
{
    public function testSum()
    {
        $list = [-4,4,7];
        $res = MathUtils::sum($list);
        self::assertEquals(7,$res);
    }

    public function testSolveLinearInvalidArg()
    {
        $this->expectException(\InvalidArgumentException::class);
        MathUtils::solveLinear(0,1);
    }

    public function testSolveLinearFloats()
    {
        self::assertEqualsWithDelta(0.5,MathUtils::solveLinear(0.2,-0.1),0.0001);
    }

    public function testSolveQuadratic()
    {
       self::assertEquals([3,-2],MathUtils::solveQuadratic(1,-1,-6));
    }

    public function testSolveQuadraticInvalidArg()
    {
        $this->expectException(\InvalidArgumentException::class);
        MathUtils::solveQuadratic(0,0,0);
    }

    public function testSolveQuadraticNoResult()
    {
        $this->expectException(NoResultException::class);
        MathUtils::solveQuadratic(1,0,1);
    }
}
