<?php


namespace Books\db;


use PDO;

class Db
{
    protected static $pdo = null;

    public static function get(): PDO
    {
        return self::$pdo ?? (self::$pdo = new PDO(
                'sqlite:books.db',
                null,
                null,
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ]
            ));
    }

    public static function initDb(){
        $db=Db::get();
        $db->query(
            'CREATE TABLE IF NOT EXISTS `book` (
            `id`          INTEGER    PRIMARY KEY AUTOINCREMENT,
            `name`        TEXT       NOT NULL,
            `author`      TEXT       NOT NULL,
            `publisher`   TEXT       NOT NULL,
            `isbn`        TEXT       NOT NULL,
            `pages`       INTEGER    NOT NULL
        )');
    }

    public static function getBooks(){
        $db = DB::get();
        $data = $db -> query("SELECT * FROM `book`")->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public static function getBookById($id){
        $db = DB::get();
        $stat = $db -> prepare("SELECT * FROM `book` where id=?");
        $stat->execute([$id]);
        $data = $stat -> fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public static function insertBook($data){
        $db = DB::get();
        $stat = $db ->prepare('INSERT INTO `book`(`name`,`author`,`publisher`,`isbn`,`pages`) VALUES (?,?,?,?,?)');
        $stat -> execute([$data['name'],$data['author'],$data['publisher'],$data['isbn'],$data['pages']]);
        return $db->lastInsertId();
    }

    public static function deleteBook($id){
        $db = DB::get();
        $stat = $db ->prepare('DELETE FROM `book` WHERE id=?');
        $stat -> execute([$id]);
        return $stat->rowCount()>=1;
    }

    public static function updateBook($id, $data){
        $db = DB::get();
        $stat = $db ->prepare('UPDATE `book` SET `name`=?, `author`=?, `publisher`=?, `isbn`=?, `pages`=? WHERE id=?');
        $stat -> execute([$data['name'],$data['author'],$data['publisher'],$data['isbn'],$data['pages'],$id]);
        return $stat->rowCount()>=1;
    }
}