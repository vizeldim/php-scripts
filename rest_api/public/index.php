<?php

use Books\db\Db;
use Books\Middleware\JsonBodyParserMiddleware;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response as EmptyResponse;

require __DIR__ . '/../vendor/autoload.php';

//SECURITY
$securityMiddleware=function (Request $request, RequestHandler $handler){
    $response = new EmptyResponse();

    if($_SERVER['PHP_AUTH_USER']!=='admin' || $_SERVER['PHP_AUTH_PW']!=='pas$word')
        $response=$response->withStatus(401);
    else
        $response = $handler->handle($request);

    return $response;
};

function invalid($data){
    return  !isset($data["name"]) ||
            !isset($data["author"]) ||
            !isset($data["publisher"]) ||
            !isset($data["isbn"]) ||
            !isset($data["pages"]);
}

Db::initDb();
$app = AppFactory::create();

$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);
$app->add(new JsonBodyParserMiddleware());

$app->get('/books', function (Request $request, Response $response, $args) {
    $payload = json_encode(Db::getBooks());
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
});


$app->get('/books/{id}', function (Request $request, Response $response, $args) {
    $id=$request->getAttribute('id');

    $data = Db::getBookById($id);
    if ($data===FALSE)
    {
        return $response->withStatus(404);
    }

    $payload = json_encode($data);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
});


$app->post('/books', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (invalid($data))
    { return $response->withStatus(400); } //Bad Request

    $id=Db::insertBook($data);
    return $response->withAddedHeader("Location", "/books/" . $id)->withStatus(201);;
})->add($securityMiddleware);


$app->delete('/books/{id}', function (Request $request, Response $response, $args) {
    $id=$request->getAttribute('id');
    $response = new EmptyResponse();
    if (Db::deleteBook($id)===TRUE)
        return $response->withStatus(204);

    return $response->withStatus(404);
})->add($securityMiddleware);

$app->put('/books/{id}', function (Request $request, Response $response, $args) {
    $id=$request->getAttribute('id');
    $data = $request->getParsedBody();
    if (invalid($data))
    {
        return $response->withStatus(400);
    }

    if (Db::updateBook($id,$data)===TRUE){
        return $response->withStatus(204);
    }

    return $response->withStatus(404);
})->add($securityMiddleware);


$app->run();